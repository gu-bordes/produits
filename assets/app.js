import './bootstrap.js';
import * as Turbo from '@hotwired/turbo';
/*
 * Welcome to your app's main JavaScript file!
 *
 * This file will be included onto the page via the importmap() Twig function,
 * which should already be in your base.html.twig.
 */
import './styles/app.css';
import 'bootstrap/dist/css/bootstrap.min.css'

/** BS DROPDOWNS **/
import { Dropdown } from "bootstrap";
const dropdownElementList = document.querySelectorAll('.dropdown-toggle')
const dropdownList = [...dropdownElementList].map(dropdownToggleEl => new Dropdown(dropdownToggleEl))

    console.log('This log comes from assets/app.js - welcome to AssetMapper! 🎉');
