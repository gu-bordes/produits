<?php

namespace App\Controller;

use App\Repository\ProduitRepository;
use Pagerfanta\Doctrine\ORM\QueryAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapQueryParameter;
use Symfony\Component\Routing\Attribute\Route;

class ProduitController extends AbstractController
{
    #[Route('/produits', name: 'app_produits')]
    public function index(
        ProduitRepository $produitRepository,
        #[MapQueryParameter] int $page = 1,
        #[MapQueryParameter] string $sort = 'id',
        #[MapQueryParameter] string $sortDirection = 'ASC'
    ): Response
    {
        $validSorts = ['id','libelle'];
        $sort = in_array($sort, $validSorts) ? $sort : 'id';
        $qb = $produitRepository->searchByParams();
        $pager = Pagerfanta::createForCurrentPageWithMaxPerPage(
            new QueryAdapter($qb->orderBy('p.' . $sort, $sortDirection)), $page, 10
        );
        return $this->render('produit/index.html.twig', [
            'produits' => $pager,
            'sort' => $sort,
            'sortDirection' => $sortDirection
        ]);
    }
}
