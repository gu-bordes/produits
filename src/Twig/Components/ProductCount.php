<?php

namespace App\Twig\Components;

use App\Repository\ProduitRepository;
use Symfony\UX\TwigComponent\Attribute\AsTwigComponent;

#[AsTwigComponent]
final class ProductCount
{
    public function __construct(private ProduitRepository $produitRepository){}

    public function getNbProduits()
    {
        return $this->produitRepository->count();
    }

}
