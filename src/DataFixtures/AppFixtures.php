<?php

namespace App\DataFixtures;

use App\Entity\Categorie;
use App\Entity\Produit;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {

        $c1 = new Categorie();
        $c1->setLibelle("Catégorie 1");
        $manager->persist($c1);

        $c2 = new Categorie();
        $c2->setLibelle("Categorie 2");
        $manager->persist($c2);

        $p1 = new Produit();
        $p1->setLibelle("Produit 1");
        $p1->setCategorie($c1);
        $p1->setDescription("Description 1");
        $p1->setDisponible(true);
        $p1->setPrixUnitaireHT(100.50);
        $manager->persist($p1);

        $p2 = new Produit();
        $p2->setLibelle("Produit 2");
        $p2->setCategorie($c2);
        $p2->setDescription("Description 2");
        $p2->setDisponible(true);
        $p2->setPrixUnitaireHT(200.50);
        $manager->persist($p2);

        for ($i = 3; $i <= 100; $i++) {
            $p = new Produit();
            $p->setLibelle("Produit " . $i);
            $p->setCategorie($c2);
            $p->setDescription("Description ". $i);
            $p->setDisponible(true);
            $p->setPrixUnitaireHT(rand(0,300));
            $manager->persist($p);
        }

        $manager->flush();
    }
}
